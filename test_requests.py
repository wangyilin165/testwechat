import requests
import pytest

class TestWechatApi:
    data = {
        'corpid': 'ww4af18a28b908d48b',
        'corpsecret': 'vmJOVzIEZ-jv9JM7SGCEGTpP7Qs3lhUtzH18iFsPqC0'
    }
    host = "https://qyapi.weixin.qq.com"

    def setup(self):
        r = requests.get("{}/cgi-bin/gettoken".format(self.host), params=self.data)
        self.token_contact = r.json()["access_token"]

    def test_api(self):
        create={
        'access_token':self.token_contact,
        'userid':'yilin123',
        'name':'yilin123',
        'mobile':"13338891115",
        'department': [1]
        }
        r1=requests.post("{}/cgi-bin/user/create?access_token={}".format(self.host,self.token_contact),json=create)
        if r1.json()["errcode"]==60104:
            print("mobile existed")
        else:
            print("create successfully!")
        update={
            'userid': 'yilin123',
            'name': 'yilin456',
            'mobile': "13338891115",
        }
        r3=requests.post("{}/cgi-bin/user/update?access_token={}".format(self.host,self.token_contact),json=update)
        print(r3.json())

        check={
            'userid': 'yilin123'
        }
        r4=requests.get("{}/cgi-bin/user/get?access_token={}".format(self.host,self.token_contact),params=check)
        print(r4.json()['name'])

        delete_user={
                 'userid':'yilin123'
                 }
        r2=requests.get("{}/cgi-bin/user/delete?access_token={}".format(self.host,self.token_contact,),params=delete_user)
        if r2.json()["errcode"]==0:
            print("deleted!")
        else:
            print("other fails")