import yaml

def test_yaml():
    test_data= {
        "method": "POST",
        "url": "https://qyapi.weixin.qq.com/cgi-bin/externalcontact/add_corp_tag?access_token={}",
        "json": {
                "group_name": "First_try",
                "tag": [{"name":"123"}]
        }
    }

    with open("tag.add.yaml","w",encoding='utf-8') as f:
        yaml.safe_dump(data=test_data,stream=f,allow_unicode=True)