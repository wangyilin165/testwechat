import requests
from TestWechatApi.api.base_api import BaseApi

class Operate(BaseApi):
    #access token
    def get_token(self,secret):
        data={
            "method":"GET",
            "url":"https://qyapi.weixin.qq.com/cgi-bin/gettoken",
            "params":{
                 'corpid': 'ww4af18a28b908d48b',
                 'corpsecret': secret
            }
        }
        return self.send_api(data)["access_token"]

