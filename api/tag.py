import requests
from TestWechatApi.api.base_api import BaseApi
from TestWechatApi.api.operator import Operate

class Tag(BaseApi):
    def __init__(self):
        self.secret = "TVSEzjXDyb1RBpPzsQ_mzt5szEroMPQg-SqlXVAzlNE"
        self.token=Operate().get_token(self.secret)

    def create_tag(self,name):
        #Method 1
        # data={
        #             "method": "POST",
        #             "url": "https://qyapi.weixin.qq.com/cgi-bin/externalcontact/add_corp_tag?access_token={}".format(self.token),
        #             "json": {
        #                 "group_name": "First_try",
        #                 "tag": [{"name": name}]
        #                 }
        #             }
        #Method2
        # data=self.load("../api/tag.add.yaml")
        # data["params"]["access_token"]=self.token
        # data["json"]["tag"][0]["name"] = name
        # return self.send_api(data)

       data=self.substitute("../api/tag.add.yaml",{"token":self.token,"tag_name":name})
       return self.send_api(data)

    def get_tag(self,tag_id=None):
        # data={
                #     "method": "POST",
                #     "url": "https://qyapi.weixin.qq.com/cgi-bin/externalcontact/get_corp_tag_list?access_token={}".format(self.token),
                #     "json": {
                #             "tag_id": []
                #             }
                #     }
        data=self.load("../api/tag.get.yaml")
        data["params"]["access_token"]=self.token
        return self.send_api(data)

    def update_tage(self,tag_id,group_name):
        data={
            "method": "POST",
            "url": "https://qyapi.weixin.qq.com/cgi-bin/externalcontact/edit_corp_tag?access_token={}".format(self.token),
            "json": {
                        "id": tag_id,
                        "name": group_name,
                  }
            }
        return self.send_api(data)

    def delete_tag(self,tagid):
        data={
            "method": "POST",
            "url": "https://qyapi.weixin.qq.com/cgi-bin/externalcontact/del_corp_tag?access_token={}".format(self.token),
            "json": {
                "tag_id": [tagid]
                }
            }
        return self.send_api(data)