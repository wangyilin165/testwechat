from string import Template
import requests
import yaml
from jsonpath import jsonpath
from pprint import pprint

class BaseApi:
    #use 【request】 to finish the transform of various requests...
    def send_api(self,req:dict):
        pprint(req)
        r=requests.request(**req)
        print(r.json())
        return r.json()

    @classmethod
    def jsonpath(cls,json,expr):
        return jsonpath(json,expr)

    def load(self,path):
        with open(path,'r') as f:
            return yaml.safe_load(f)

    def substitute(self,path,data):
        with open(path,'r') as f:
            return yaml.load(Template(f.read()).substitute(data))

