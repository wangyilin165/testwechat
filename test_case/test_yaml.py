import yaml

from TestWechatApi.api.base_api import BaseApi


def test_yaml():
    test_data=[
        ("黄宗泽2","黄宗泽123"),
        ("卫兰2", "卫兰123")
    ]

    with open("test_tag.data.yaml","w",encoding='utf-8') as f:
        yaml.safe_dump(data=test_data,stream=f,allow_unicode=True)

def test_template():
    # with open("../api/tag.add.yaml","r") as f:
    #     print(yaml.load(stream=f))
    print(BaseApi().substitute("../api/tag.add.yaml", {"token": "123", "tag_name": "demo"}))