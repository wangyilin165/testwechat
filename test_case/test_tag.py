import pytest

from TestWechatApi.api.base_api import BaseApi
from TestWechatApi.api.tag import Tag

class TestTag:
    test_data=BaseApi().load("test_tag.data.yaml")

    @classmethod
    def setup_class(cls):
        cls.tag = Tag()

    def setup(self):
        pass

    @pytest.mark.parametrize("name_old,name_new",test_data)
    def testAll(self,name_old,name_new):
        value = self.tag.get_tag()
        #name_list=self.tag.jsonpath(value,'$..tag..name')
        #print(name_list)
        for xname in [name_old,name_new]:
            tag_id=self.tag.jsonpath(json=value,expr=f'$..tag[?(@.name=="{xname}")].id')
            print(xname)
            print(tag_id)
            if tag_id:
                self.tag.delete_tag(tag_id[0])
            else:
                print("this tag does not exist")

        assert self.tag.create_tag(name_old)["errcode"]==0
        tag_id=self.tag.jsonpath(self.tag.get_tag(),f'$..tag[?(@.name=="{name_old}")].id')
        #print(tag_id)
        #print(self.tag.jsonpath(self.tag.get_tag(),'$..tag..name'))
        assert self.tag.update_tage(tag_id[0],name_new)["errcode"]==0
        #print(self.tag.jsonpath(self.tag.get_tag(),'$..tag..name'))


    def test_createTag(self):
        assert self.tag.create_tag(name="important3")["errcode"]==0
        # r2=self.tag.create_tag(name="medium")
        # r2=self.tag.create_tag(name="low")

    #@pytest.mark.skip
    def test_getTag(self):
        assert self.tag.get_tag()["errcode"]==0

    @pytest.mark.skip
    def test_updateTag(self):
        r=self.tag.update_tage(tag_id="eto_69BgAAZptgg-HlMuC67fi0NMHclA",group_name="second_try")
        print(r)

    @pytest.mark.skip
    def test_deltag(self):
        r=self.tag.delete_tag('eto_69BgAA6fgqPz5nIODAzq_vgl9zfQ')
        print(r)

    @pytest.mark.skip
    def test_combine(self):
        #r1=self.tag.create_tag(name="important")
        value = self.tag.get_tag()
        print(self.tag.jsonpath(value,'$..tag'))
        tag_value1=self.tag.jsonpath(json=value,expr='$..tag[?(@.name=="second_try")].id')
        print(tag_value1)
        if tag_value1:
            self.tag.delete_tag(tag_value1)
            print(self.tag.delete_tag(tag_value1))
            print("delete successfully")
        self.tag.create_tag(name="张敬轩")
        print("create successfully")
        tag_value2=self.tag.jsonpath(value,'$..tag[?(@.name=="张敬轩")].id')[0]
        print(tag_value2)
        self.tag.update_tage(tag_value2, "hinhin张敬轩")
        print(self.tag.update_tage(tag_value2, "hinhin张敬轩"))
        print(self.tag.jsonpath(value,'$..tag'))
        # print(self.tag.get_tag())




